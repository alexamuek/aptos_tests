import { Aptos, AptosConfig, Network, Account, Ed25519PrivateKey, TransactionResponse, U64, InputViewRequestData } from "@aptos-labs/ts-sdk";
//import {wrapper as w} from "./data.js";
const w = require('./data1');

jest.setTimeout(30000)

test("test_1", async () => {

  const wrapper = w.wrapper;
  const erc721mock = w.erc721mock;
  //const aptos = new Aptos(); // default to devnet

  // with custom configuration - connect to nestnet
  const aptosConfig = new AptosConfig({ network: Network.TESTNET });
  const aptos = new Aptos(aptosConfig);

  //const { privateKey: privateKeyBytes, publicKey, address } = ed25519;
  //add accounts
  let privateKey = new Ed25519PrivateKey('0x289571f4b4bc4a420e14ce66d1959c0dc9c802d33d2688b25b86cb6f073195cc');
  const newAccount = Account.fromPrivateKey({ privateKey });
  expect(newAccount).toBeInstanceOf(Account);
  expect(newAccount.privateKey.toString()).toEqual('0x289571f4b4bc4a420e14ce66d1959c0dc9c802d33d2688b25b86cb6f073195cc')

  //save before balance info for wrapper
  let data = await aptos.getAccountCoinsData({accountAddress: wrapper})
  let before_fungable_token_balance = (data as any)[1].amount;
  console.log('before_fungable_token_balance = ', before_fungable_token_balance);
  let before_native_token_balance = (data as any)[0].amount;

  
  //mint original erc721
  let contract = `${erc721mock}::${'tech_nft'}::${'mint_nft'}`
  const rawTxn = await aptos.transaction.build.simple({
    sender: newAccount.accountAddress,
    data: {
      function: (contract as any),
      functionArguments: [],
    },
  });
  const authenticator = aptos.transaction.sign({
    signer: newAccount,
    transaction: rawTxn,
  });
  const response = await aptos.transaction.submit.simple({
    transaction: rawTxn,
    senderAuthenticator: authenticator,
  });

  //console.log(response)
  //console.log(response.hash)
  
  let txn: TransactionResponse;

  txn = await aptos.waitForTransaction({ transactionHash: response.hash })
  

  //console.log(txn)
  //console.log(typeof(txn))
  //console.log(Object.keys(txn))
  let transaction = await aptos.getTransactionByHash({
      transactionHash: txn.hash,
    });

  let event = (transaction as any).events.find((item: any) => { return item.type === '0x4::collection::MintEvent' });
  
  //save original token before
  const token = event.data.token;
  //const token = '"0x725abce4fcf8c715810eefd5e9775995725ad3f7790827c340954a8b121f1b47"';
  console.log('original_token = ', token);

  //let tokens = await aptos.getAccountOwnedTokens({ accountAddress: newAccount.accountAddress });
  //const tokens1 = await aptos.digitalAsset.getCurrentTokenOwnership({ tokenAddress: "0x2b7802d7d8ef559c431e1a43e982ef19db4c19ae713449529f18dee85130454d" })
  //console.log(tokens)

  //wrap without timelock!!!!!!!!!!!!!
  //let contract = "0xc259d2b1f704bd49d2fc7f5bd150c89ac74514ef970bf0b2c56188a9b823eddb::env_wrapper::wrap";
  contract = `${wrapper}::${'env_wrapper'}::${'wrap'}`
  const rawTxn1 = await aptos.transaction.build.simple({
    sender: newAccount.accountAddress,
    data: {
      function: (contract as any),
      functionArguments: [token, new U64(0), new U64(0)], 
    },
  });
  const authenticator1 = aptos.transaction.sign({
    signer: newAccount,
    transaction: rawTxn1,
  });
  const response1 = await aptos.transaction.submit.simple({
    transaction: rawTxn1,
    senderAuthenticator: authenticator1,
  });

  //console.log('wrap_tx = ', response1)
  //console.log(response1.hash)
  

  txn = await aptos.waitForTransaction({ transactionHash: response1.hash })
  //console.log(txn)
  event = (txn as any).events.find((item: any) => { return item.type === '0x4::collection::MintEvent' });
  
  //save wnft id info
  const wnft = event.data.token;
  //const wnft = '0xda55a3edb97253128577d0f393779fffaebeef3a7df1e40175904e4e0875659a'

  console.log('wnft = ', wnft);
  

  let tokens = await aptos.getAccountOwnedTokens({ accountAddress: newAccount.accountAddress });
  //console.log(tokens);

  
  //wnft
  //console.log(await aptos.digitalAsset.getCurrentDigitalAssetOwnership({digitalAssetAddress: '0x45ff4f103c40259ec3ae8dc3a0a1cd254fbdd3508ed53e35254099aebe6541c6'}))
  //orig
  //console.log(await aptos.digitalAsset.getCurrentDigitalAssetOwnership({digitalAssetAddress: '0xadf25f24432badd04a6ec59d36d32a0f68fffc9e0cea8866da9fecef9e43bd17'}))

  //let tokens = await aptos.getAccountOwnedTokens({ accountAddress: wrapper });
  //console.log(tokens)

  //let tokens = await aptos.getAccountOwnedTokens({ accountAddress: newAccount.accountAddress });
  //console.log(tokens)

  //create new account and get his data
  //const senderAccount = Account.generate();
  //await aptos.fundAccount({
  //    accountAddress: senderAccount.accountAddress,
  //    amount: 100_000_000,
  //});

  //console.log(senderAccount.privateKey.toString());
  //console.log(senderAccount.publicKey.toString());
  //console.log(senderAccount.accountAddress.toString());

  
  //try to unwrap by now owner

  //add new account
  privateKey = new Ed25519PrivateKey('0x4e5e055a77c1b5108b5bc715bf69daea5ec852376709449142564d4f866b2ef3');
  const senderAccount = Account.fromPrivateKey({ privateKey });
  expect(senderAccount).toBeInstanceOf(Account);
  expect(senderAccount.accountAddress.toString()).toEqual('0xf66de3221623048f17fb4d5e8554ff8b9cab2deac8a014c0746eea74aa6d0ff2')

  contract = `${wrapper}::${'env_wrapper'}::${'unwrap'}`
  const rawTxn2 = await aptos.transaction.build.simple({
    sender: senderAccount.accountAddress,
    data: {
      function: (contract as any),
      functionArguments: [wnft], 
    },
  });
  const authenticator2 = aptos.transaction.sign({
    signer: senderAccount,
    transaction: rawTxn2,
  });
  const response2 = await aptos.transaction.submit.simple({
    transaction: rawTxn2,
    senderAuthenticator: authenticator2,
  });

  console.log('unwrap_tx = ', response2)
  console.log(response2.hash)

  const transaction_fail = await aptos.getTransactionByHash({
      transactionHash: response2.hash,
    });

  //console.log(transaction_fail)
  

  try {
    txn = await aptos.waitForTransaction({ transactionHash: response2.hash })
  }
  catch(e) {
      console.log((e as Error).message);
  }

  /*let data = await aptos.getFungibleAssetMetadataByAssetType({ assetType: '0xb737ce4c9c9d0ca51456c26fa31d1966cf51fccda8f3a37f8588984311eda1d6' });
  console.log(data)

  let data1 = await aptos.getCurrentFungibleAssetBalances({
    options: {
      where: {
        owner_address: { _eq: '0xf66de3221623048f17fb4d5e8554ff8b9cab2deac8a014c0746eea74aa6d0ff2' },
        asset_type: { _eq: '0xb737ce4c9c9d0ca51456c26fa31d1966cf51fccda8f3a37f8588984311eda1d6' },
      },
    },
  });
  console.log(data1)*/

  ///////////////////////
  //add collateral erc20
  ///////////////////////
  contract = `${wrapper}::${'env_wrapper'}::${'add_fa_collateral'}`
  const rawTxn4 = await aptos.transaction.build.simple({
    sender: senderAccount.accountAddress,
    data: {
      function: (contract as any),
      functionArguments: [wnft, '0xb737ce4c9c9d0ca51456c26fa31d1966cf51fccda8f3a37f8588984311eda1d6', new U64(100)], 
    },
  });
  const authenticator4 = aptos.transaction.sign({
    signer: senderAccount,
    transaction: rawTxn4,
  });
  const response4 = await aptos.transaction.submit.simple({
    transaction: rawTxn4,
    senderAuthenticator: authenticator4,
  });

  //console.log('add_collateral_tx = ', response4)
  //console.log(response4.hash)
  

  txn = await aptos.waitForTransaction({ transactionHash: response4.hash })
  //txn = await aptos.waitForTransaction({ transactionHash: '0x6f3ff41b869e20f7fff784f1728634f10206d6b8cf45a1fb91a0c1452d385b4e' })
  //console.log(txn)
  event = (txn as any).events; //.find((item: any) => { return item.type === '0x4::collection::MintEvent' });
  //console.log('add collateral event = ',event)

  ///////////////////////
  //add collateral erc20 again
  ///////////////////////
  contract = `${wrapper}::${'env_wrapper'}::${'add_fa_collateral'}`
  const rawTxn5 = await aptos.transaction.build.simple({
    sender: senderAccount.accountAddress,
    data: {
      function: (contract as any),
      functionArguments: [wnft, '0xb737ce4c9c9d0ca51456c26fa31d1966cf51fccda8f3a37f8588984311eda1d6', new U64(50)], 
    },
  });
  const authenticator5 = aptos.transaction.sign({
    signer: senderAccount,
    transaction: rawTxn5,
  });
  const response5 = await aptos.transaction.submit.simple({
    transaction: rawTxn5,
    senderAuthenticator: authenticator5,
  });

  //console.log('add_collateral_tx = ', response5)
  //console.log(response5.hash)
  

  txn = await aptos.waitForTransaction({ transactionHash: response5.hash })
  //console.log(txn)
  event = (txn as any).events; //.find((item: any) => { return item.type === '0x4::collection::MintEvent' });
  //console.log('add collateral event = ',event)
  
  contract = `${wrapper}::${'env_wrapper'}::${'get_FA_collateral'}`
  let payload: InputViewRequestData = {
    function: (contract as any),
    functionArguments: [wnft],
  };
  let res = (await aptos.view<[{ inner: string }]>({ payload }))[0];
  //console.log(res);
  //console.log((res as any)[0].amount);
  expect((res as any)[0].amount).toEqual('150');

  //add native token to wnft
  contract = `${wrapper}::${'env_wrapper'}::${'add_native_collateral'}`
  const rawTxn6 = await aptos.transaction.build.simple({
    sender: senderAccount.accountAddress,
    data: {
      function: (contract as any),
      functionArguments: [wnft, new U64(10)], 
    },
  });
  const authenticator6 = aptos.transaction.sign({
    signer: senderAccount,
    transaction: rawTxn6,
  });
  const response6 = await aptos.transaction.submit.simple({
    transaction: rawTxn6,
    senderAuthenticator: authenticator6,
  });

  //console.log('add_collateral_tx = ', response6)
  //console.log(response6.hash)
  

  txn = await aptos.waitForTransaction({ transactionHash: response6.hash })
  //console.log(txn)
  event = (txn as any).events; //.find((item: any) => { return item.type === '0x4::collection::MintEvent' });
  //console.log('add collateral event = ',event)

  //add native token to wnft - again
  contract = `${wrapper}::${'env_wrapper'}::${'add_native_collateral'}`
  const rawTxn7 = await aptos.transaction.build.simple({
    sender: senderAccount.accountAddress,
    data: {
      function: (contract as any),
      functionArguments: [wnft, new U64(20)], 
    },
  });
  const authenticator7 = aptos.transaction.sign({
    signer: senderAccount,
    transaction: rawTxn7,
  });
  const response7 = await aptos.transaction.submit.simple({
    transaction: rawTxn7,
    senderAuthenticator: authenticator7,
  });

  //console.log('add_collateral_tx = ', response7)
  //console.log(response7.hash)
  

  txn = await aptos.waitForTransaction({ transactionHash: response7.hash })
  //txn = await aptos.waitForTransaction({ transactionHash: '0x6f3ff41b869e20f7fff784f1728634f10206d6b8cf45a1fb91a0c1452d385b4e' })
  //console.log(txn)
  event = (txn as any).events; //.find((item: any) => { return item.type === '0x4::collection::MintEvent' });
  //console.log('add collateral event = ',event)

  contract = `${wrapper}::${'env_wrapper'}::${'get_token_value'}`
  payload = {
    function: (contract as any),
    functionArguments: [wnft],
  };
  res = (await aptos.view<[{ inner: string }]>({ payload }))[0];
  //check native tokens in wnft
  //expect(res).toEqual(10);
  data = await aptos.getAccountCoinsData({accountAddress: wrapper})
  expect((data as any)[0].amount).toEqual(before_native_token_balance + 30)
  //console.log(res);
  //console.log((res as any)[0].amount);
  
  //check view functions
  contract = `${wrapper}::${'env_wrapper'}::${'get_wrapped_token'}`
  payload = {
    function: (contract as any),
    functionArguments: [wnft],
  };
  res = (await aptos.view<[{ inner: string }]>({ payload }))[0];
  //console.log(await aptos.view<[{ inner: string }]>({ payload }));
  expect((res as any).addr).toEqual(token);
  expect((res as any).backed_value).toEqual('30');
  expect((res as any).unwrap_after).toEqual('0');

  //unwrap by owner
  contract = `${wrapper}::${'env_wrapper'}::${'unwrap'}`
  const rawTxn3 = await aptos.transaction.build.simple({
    sender: newAccount.accountAddress,
    data: {
      function: (contract as any),
      functionArguments: [wnft], 
    },
  });
  const authenticator3 = aptos.transaction.sign({
    signer: newAccount,
    transaction: rawTxn3,
  });
  const response3 = await aptos.transaction.submit.simple({
    transaction: rawTxn3,
    senderAuthenticator: authenticator3,
  });

  //console.log('unwrap_tx = ', response3)
  //console.log(response3.hash)

  //const transaction3 = await aptos.getTransactionByHash({
  //    transactionHash: response3.hash,
  //  });
  const transaction3 = await aptos.waitForTransaction({ transactionHash: response3.hash })
  //console.log(transaction3)
  expect((transaction3 as any).success).toEqual(true)

  const wnft_info = await aptos.getDigitalAssetData({ digitalAssetAddress: wnft })
  console.log('wnft_info = ',wnft_info)

  
  event = (transaction3 as any).events.find((item: any) => { return item.type === '0x4::collection::BurnEvent' });
  //console.log(event)
  expect(event.data.token).toEqual(wnft)

  //mint erc20 tokens for account
  /*const rawTxn1 = await aptos.transaction.build.simple({
    sender: newAccount.accountAddress,
    data: {
      function: "0x0c2272ae7b5a0c23c21eb3620aa52f5ef0923a78bcd94b6660c0caa7d8ccea29::fa_coin::mint",
      functionArguments: ['0x9e6373a575a6c47922dadce43da8df70f60f3a6ba46384f652cb7c6674486f9c', new U64(5000)], 
    },
  });
  const authenticator1 = aptos.transaction.sign({
    signer: newAccount,
    transaction: rawTxn1,
  });
  const response1 = await aptos.transaction.submit.simple({
    transaction: rawTxn1,
    senderAuthenticator: authenticator1,
  });

  console.log('mint_tx = ', response1)
  console.log(response1.hash)
  

  let txn = await aptos.waitForTransaction({ transactionHash: response1.hash })*/

  //check fungable token balance for wrapper after unwrap
  data = await aptos.getAccountCoinsData({accountAddress: wrapper})
  //console.log(data)

  //console.log((data as any)[1].amount)
  expect((data as any)[1].amount).toEqual(before_fungable_token_balance)
  expect((data as any)[0].amount).toEqual(before_native_token_balance)

  //unwrap by owner - wnft is burnt
  contract = `${wrapper}::${'env_wrapper'}::${'unwrap'}`
  const rawTxn8 = await aptos.transaction.build.simple({
    sender: newAccount.accountAddress,
    data: {
      function: (contract as any),
      functionArguments: [wnft], 
    },
  });
  const authenticator8 = aptos.transaction.sign({
    signer: newAccount,
    transaction: rawTxn8,
  });
  const response8 = await aptos.transaction.submit.simple({
    transaction: rawTxn8,
    senderAuthenticator: authenticator8,
  });

  console.log('unwrap_tx = ', response8)
  //console.log(response3.hash)

  //const transaction8 = await aptos.getTransactionByHash({
  //    transactionHash: response3.hash,
  //  });
  try {
    txn = await aptos.waitForTransaction({ transactionHash: response8.hash })
  }
  catch(e) {
      console.log((e as Error).message);
  }
  
});


  